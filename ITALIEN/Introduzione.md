---
title: Introduction
---

Quale buon utilizzo degli strumenti digitali?<br>
Adotta dei software liberi che corrispondano ai tuoi valori
{: .slogan-subtitle }

Gli strumenti digitali più utilizzati non sono sempre i più pratici o i più etici. Quale prezzo siamo disposti a pagare per usarli? Le condizioni d'uso che talvolta consistono nel dover accettare di divulgare i dati personali e quelli dei membri della tua associazione,  sono vincoli trascurabili? Puoi utilizzare un qualsiasi servizio, libero o meno, senza considerare la coerenza con i valori sostenuti dalla tua organizzazione, dal tuo gruppo o dalla tua associazione? E anche se sei pronto a fare il grande passo per dotare i tuoi dispositivi di software libero, lo sono anche i tuoi colleghi? Hai anticipato loro questi cambiamenti?


Frutto della collaborazione tra Framasoft e CEMÉA, questa raccolta di schede presenta dei percorsi per avviare una riflessione sugli usi e le libertà digitali. Raggruppati in base ai tre temi principali di **[collaborazione](Collaborare/index.md)**, **[comunicazione](Comunicare/index.md)** e **[organizzazione](Organiizzare/index.md)**, verranno presentati alcuni strumenti per facilitare il tuo approccio e adottare soluzioni sane, basate sui software liberi.
{: .encart }

## Libero, non è solo il software

Il Libero è soprattutto un ecosistema di organizzazioni che si battono per una Internet diversa, più aperta, più sana e più trasparente. Sostenere questo ecosistema significa creare legami sociali e contribuire a una visione della società basata sulla **condivisione e lo scambio di competenze e conoscenze**. Al contrario, usare strumenti proprietari che non rispettano molto la nostra privacy digitale significa accettare un'economia in cui alcuni monopoli si accaparrano le risorse e ne  impongono gli usi. 


I software liberi sono uno dei migliori veicoli collettivi per la condivisione, la solidarietà e l'inclusione. L'economia sociale e di solidarietà, le organizzazioni di istruzione popolare e tutte le organizzazioni che lavorano per un mondo migliore dovrebbero preferire il loro uso perché nelle loro molte attività sarebbe un peccato usare strumenti che non rispettano gli utenti, violano la nostra vita privata e impongono i loro usi e la loro visione del mondo.

L'obiettivo di questo documento a più schede è quello di permettere alla tua organizzazione di fare agevolmente il passaggio da soluzioni digitali chiuse a soluzioni digitali aperte. Perché non puoi pensare a tutto. Perché, per convincere, hai bisogno di argomenti.
{: .slogan-contenu }

## La privacy su Internet

Quando si utilizza un servizio online, i nostri dati sono sul computer di qualcun  altro. Questa scelta non è senza conseguenze. Se la fai a nome della tua organizzazione e scegli un servizio fornito da Google, tutti gli aderenti saranno esposti alle pratiche discutibili di questa azienda. E anche se pensi che la scelta riguardi solo te, il semplice fatto di utilizzare alcune funzionalità (come ad esempio la condivisione di file, l’e-mail o un semplice sondaggio) può implicare che anche i tuoi corrispondenti siano esposti a loro volta alle pratiche non etiche di questo fornitore. Insomma, la   privacy **non è mai una questione di scelta individuale**!


## Liberi, veramente?

Un software libero è definito in base alle seguenti libertà, inserite in una licenza che accompagna il programma: puoi usarlo come desideri,  puoi distribuirlo a tutti, puoi persino studiare il suo codice sorgente, modificarlo e distribuire le tue modifiche.

Così, a differenza dei software proprietari, i software liberi sono beni [comuni digitali](Organizzare/comuni.md), definiti e creati da una comunità. Probabilmente ne conosci già alcuni, come Firefox o LibreOffice, ma ce ne sono molti altri! 

Questi strumenti sono in continuo sviluppo e appartengono a tutti. Quindi soddisfano meglio le aspettative degli utenti, visto che anche loro possono partecipare al processo di sviluppo.


Meglio ancora, Libero è anche un movimento che non riguarda solamente il software: si possono anche liberare le opere dell’ingegno, le conoscenze, l’arte, l’agricoltura, la medicina… insomma Libero è anche uno stato dello spirito.
{: .encart }



## I servizi web

Un servizio web è uno strumento accessibile online. È reso disponibile da un provider che si occupa della disponibilità e della manutenzione del servizio.

Alcuni servizi sono ospitati da potenti aziende come Google, Facebook o Microsoft. Si basano su programmi che non soltanto non sono liberi, ma sono progettati per catturare molte informazioni sugli utenti  in cambio di un’apparente [gratuità](Collaborare/gratuita.md). Invece altri servizi sono offerti da provider più piccoli e  a misura d’uomo che hanno fatto altre scelte: utilizzare dei software liberi e  **rispettare gli utenti** per scelta [Etica](Collaborare/etica.md).

Questo è, ad esempio, l'impegno degli CHATONS (Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires – [chatons.org](https://chatons.org)! Queste strutture offrono servizi etici e accompagnano gli utenti nel **riprendere il controllo** sui loro strumenti.


## Ma è gratis?

Il software libero è spesso gratuito. 
O almeno è già pagato dai contributori che hanno speso il loro tempo e  il loro denaro per renderlo disponibile. L’hosting invece non è sempre così. Effettivamente, mantenere un servizio online richiede  del materiale, a volte costoso  e del tempo, è per questo che alcuni provider  richiedono una tariffa o un contributo per il servizio che offrono. **[→ Gratuità](Collaborare/gratuita.md)**.


## Contribuisci anche tu e diffondi queste schede!

Queste schede sono distribuite con licenza  CC-by-sa, puoi riutilizzarle, modificarle e redistribuirle come vuoi.
{: .slogan-contenu }


