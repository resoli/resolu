---
title: La privacy
---

Perché la privacy è importante?
Come utilizzare gli strumenti digitali rispettando la propria vita privata e quella degli altri?
{: .slogan-subtitle }

## Cosa si intende con dato personale?

I dati personali sono tutte le informazioni che riguardano una persona fisica. Permettono di identificarla in maniera diretta (il nome e il cognome) o in maniera indiretta (un indirizzo postale, un numero di telefono, un indirizzo IP, ecc.)

Si può identificare qualcuno raccogliendo dati personali grazie a diverse tecniche di controllo incrociato dei dati comportamentali (compresi i comportamenti in Internet) e incrociando informazioni apparentemente insignificanti (tra cui i dati di clientela, le iscrizioni a servizi digitali online, ecc.).

## Perché preoccuparsene?

I dati personali sono molto utili per aziende come i giganti del Web (ad esempio i famosi GAFAM), dal momento che per loro si tratta del reale prodotto. Loro scopo non è di fornire un servizio che conviene all’utilizzatore, ma di concepire servizi che gli permettano di **raccogliere quante più informazioni personali è possibile**. Questi dati sono tipicamente rivenduti agli inserzionisti che contribuiscono a un’economia basata sulla pubblicità.

## Saper scegliere

La scelta delle applicazioni e dei servizi che utilizzate nella vostra azienda è di fatto una scelta strategica. **Dovete proteggere** i dati dei vostri iscritti, dei visitatori dei vostri siti web o degli utilizzatori dei vostri servizi. Non è solo un dovere reso obbligatorio dalla legge, ovvero dal Regolamento Europeo per la Protezione dei Dati Personali (GDPR), ma anche un dovere morale che consiste nel non esporre l’intimità dei vostri collaboratori o visitatori (e la legge non definisce tutto). Dovete quindi assicurarvi del grado di **fiducia** che riconoscete alle soluzioni digitali che utilizzate e del grado di **fiducia** che riponete negli hosting o fornitori con cui lavorate.

## «Ok, ma non mi disturba»…

Tutto ciò può d’altro canto diventare un problema **per altre** persone coinvolte, che siano persone della struttura o persone esterne che abbiano trasmesso le proprie informazioni. Per esempio, per un’associazione di aiuto a persone in difficoltà sociale o di salute, o per un’associazione di militanti politici, consegnare le proprie informazioni ad attori privati può essere problematico.

## … oppure : «non ho nulla da nascondere»

Quando andate in bagno chiudete la porta. Tutti abbiamo **un’intimità** e potenzialmente ciascuno ha una **intimità digitale**. L’intimità è un bisogno vitale e partecipa alla nostra realizzazione in quanto esseri umani.

Proteggere la propria intimità digitale significa anche proteggere l’intimità altrui.
{: .slogan-contenu }

Tutte le tracce informatiche che lasciamo possono essere analizzate in modo automatico da processi di data mining su larga scala realizzati da aziende che raccolgono masse di dati , elaborano dei profili e sono in grado di orientare i nostri comportamenti economici e sociali, come pure le nostre scelte politiche (a questo proposito, si faccia riferimento all’affare Cambridge Analytica esploso nel 2018).
