---
title: Comunicare facilmente con Mattermost
---

Messaggistica istantanea ottimizzata per i gruppi
{: .slogan-subtitle }

Mattermost è una soluzione di **comunicazione di gruppo** adatta alle piccole e grandi strutture.

Un'organizzazione basata sui gruppi e un'interfaccia semplice e intuitiva permettono di scambiare informazioni attraverso diversi canali pubblici o privati per strutturare le conversazioni e non perdere informazioni importanti.

Tutte le vostre discussioni organizzate e nello stesso luogo!
{: .slogan-contenu }

È possibile creare diversi canali e gestirne l'accesso. Questi canali consentono di separare le conversazioni in base agli argomenti e alle persone coinvolte, pur rimanendo sullo stesso strumento. Questo permette di contattare rapidamente le persone coinvolte in un argomento, facilita la ricerca nello storico e così via.

Per organizzarsi meglio, è possibile raggruppare i canali e gestire più finemente i diritti di accesso.

## Mantenere il controllo sui propri dati

Mattermost è un programma libero. Potete installarlo voi stessi o utilizzare un host etico. I vostri dati saranno poi gestiti in modo rispettoso e trasparente da qualcuna/o di cui vi fidate.

## « Ma le e-mail vanno bene »... 

Sì, ma non sempre è il modo più appropriato per comunicare. Mentre la posta elettronica è molto comoda per l'invio di messaggi scritti per la corrispondenza con latenze elevate, non sono affatto adatti per scambi brevi, soprattutto per molti. I ritardi sono maggiori, i messaggi vengono copiati su server diversi e quindi consumano più risorse, e spesso si finisce per ricevere una montagna di e-mail che non ci riguardano.

Con Mattermost, le conversazioni sono centralizzate su un unico server, perciò non ci sono copie inutili dei dati. Chiunque può, a seconda del livello di coinvolgimento desiderato, scegliendo se leggere o meno ciò che non gli è indirizzato direttamente.

## E molte altre funzionalità

Oltre alle funzioni di conversazione istantanea e strutturata, è possibile inviare file, rispondere a un messaggio per tenere traccia del filo di una discussione o reagire rapidamente a un messaggio con degli emoticon. Inoltre, Mattermost offre la possibilità di aggiungere **estensioni** che consentono, ad esempio, di effettuare sondaggi, integrare videoconferenze, ecc.

## Una interfaccia intuitiva e completa

Accessibile da un browser web, da un'applicazione per computer o da un'applicazione mobile, l'interfaccia di Mattermost è altamente personalizzabile e facile da usare. 

La separazione dei gruppi, delle stanze e dei messaggi personali permette di avere rapidamente una visione d'insieme delle diverse conversazioni.

## Adesso tocca a te!

Iniziare a usare Mattermost per comunicare all'interno della vostra organizzazione è facile. Ci sono tre modi per farlo:

-   Passare attraverso un hosting etico che offre un'istanza aperta, come  [framateam.org](http://framateam.org/) o
    [team.picasoft.net](http://team.picasoft.net/), o altri CHATONS per esempio.
-   Ospitare la propria istanza. Richiede poche risorse ma alcune competenze.
-   Utilizzare la versione Enterprise di Mattermost. Non è gratuito, ma offre più funzionalità per le grandi strutture.

## Confronto con altre soluzioni

In questo contesto vengono talvolta utilizzati Facebook Messenger e WhatsApp. Tuttavia, non sono strumenti di lavoro, ma strumenti di comunicazione progettati per le discussioni tra amici o familiari. Oltre a non offrire caratteristiche utili per il lavoro collaborativo, questi strumenti sono spesso utilizzati anche in ambito privato. Ciò rende più difficile separare le attività professionali o associative da quelle personali.

Slack è simile a Mattermost per molti aspetti. L'interfaccia è simile e gli usi sono molto simili. Tuttavia, Slack non può essere ospitato da nessun altro che non sia l'azienda che lo sviluppa. Questo centralizza le risorse e i dati nelle mani di un solo attore che può cambiare la sua politica in qualsiasi momento. Inoltre, la versione a pagamento di Mattermost offre più possibilità che la versione a pagamento di Slack, ad esempio in termini di personalizzazione, gestione dei dati e trasparenza.

## Buone pratiche

Le conversazioni su Mattermost possono diventare leggermente caotiche se non si seguono certe pratiche. Per esempio, è una buona idea usare la funzione "risposta" per tenere traccia di una conversazione, usare un chiaro nome utente o specificare il proprio nome in modo da essere facilmente identificabili e cercare di trovare un equilibrio tra molte stanze troppo specifiche e poche stanze troppo generiche.

## Loro l'hanno fatto!

Molte associazioni e piccole imprese hanno già fatto il grande passo, ma non sono le uniche. CERN, Uber, ING, Intel, Dipartimento della Difesa degli Stati Uniti, Mozilla, National University of Singapore e molte altre grandi organizzazioni supportano Mattermost utilizzando la versione Enterprise.
