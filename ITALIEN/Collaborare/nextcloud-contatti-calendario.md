---
title: Nextcloud -- Contatti & Calendario
---

Sincronizzare e condividere i contatti: uno strumento strategico
{: .slogan-subtitle }

È possibile usare le applicazioni Nextcloud in modo strettamente individuale, ma l'interesse di questa soluzione aumenta notevolmente quando l'uso è collettivo.

## Le applicazioni

Le applicazioni Contatti e Calendario sono applicazioni installate automaticamente in Nextcloud. Naturalmente, a seconda dell'istanza in cui si ha un account, saranno più o meno accessibili. Tutto dipende da come le persone che gestiscono l'istanza hanno configurato Nextcloud per i loro utenti.

Spetta invece a te configurare le applicazioni sul tuo computer o sul tuo smartphone che saranno in grado di connettersi alle applicazioni di contatti e  calendario dell'istanza di Nextcloud. Il principio è sincronizzare i tuoi dati in modo che tutti i tuoi dispositivi siano collegati. 

## Sincronizzare

La sincronizzazione è abbastanza facile da configurare su più dispositivi. Si applica alla gestione dei contatti su uno smartphone, all'elenco dei contatti di un client di posta elettronica su un computer o nell'interfaccia web di Nextcloud. Allo stesso modo, la gestione del tuo impiego del tempo, dei tuoi appuntamenti e di tutti gli inviti, può essere gestita indifferentemente tramite i tuoi dispositivi o l'interfaccia web. Anche le attività possono essere sincronizzate (con l'applicazione Attività di Nextcloud o OpenTasks su smartphone). 

Per questa configurazione, consulta le istruzioni per l'uso dell'istanza o il manuale Nextcloud. In sostanza, si tratta di connettersi tramite un protocollo (WebDAV), utilizzabile con qualsiasi sistema operativo.
{: .encart }

Il vantaggio della sincronizzazione dei contatti su più dispositivi consiste nel non limitare l'uso del tuo elenco contatti a un solo dispositivo. Inoltre,  un errore comune è quello di creare contatti quando si presenta la necessità, indipendentemente dal dispositivo, il che crea numerose ridondanze ed errori e rende difficile la gestione dei contatti.

Per quanto riguarda il calendario, è molto utile poterlo avere a portata di mano sia quando utilizzi il computer sia  quando sei in movimento e  hai solo il tuo smartphone. In entrambi i casi sai che le tue informazioni saranno le stesse senza doverle copiare o  dover collegare "manualmente" i tuoi dispositivi.

## Collaborare

Attraverso un account, puoi sincronizzare i tuoi contatti, tenerli per te o creare un elenco di contatti da condividere con i tuoi collaboratori. Puoi anche usare la tua sessione con il tuo calendario e condividere solo determinati eventi.

Nextcloud funziona con una gestione precisa dei diritti di accesso. È quindi possibile, in un gruppo, non solo condividere un elenco di contatti, ma anche determinare chi ha accesso a un elenco o ad un altro. Ogni modifica sarà sincronizzata tra tutti i membri del gruppo: addio contatti incompleti o dimenticati, foglietti di carta smarriti. La gestione del calendario obbedisce agli stessi principi, in particolare quando i contatti e il calendario interagiscono: gestire le ferie, organizzare riunioni, creare degli inviti, condividere un'agenda pubblica, organizzare le presenze.

Tutto questo permette ad un gruppo di gestire l’impiego del tempo senza dover moltiplicare gli scambi tramite la messaggistica.

## Buone pratiche

Quando collabori con altre persone, ricordati che non tutti i tuoi collaboratori attribuiscono sempre la stessa rilevanza a ogni informazione. Se organizzi una riunione, il suo scopo, gli orari e i nomi dei partecipanti sono tre informazioni differenti. Allo stesso modo, una scheda di contatto può contenere più informazioni su una persona (cognome, nome, numero di telefono personale o professionale, indirizzo e-mail personale o professionale, indirizzo postale, ecc.). Non tutte le informazioni devono necessariamente essere conosciute da tutti.

Quindi, quando condividi i contatti o un calendario, assicurati che gli altri utenti ricevano un livello di informazioni adeguato: la scelta di condividere tutto in maniera indifferenziata per motivi di velocità può avere gravi conseguenze. Nextcloud ti permette di gestire correttamente  e con grande facilità i diritti di accesso individualmente o per gruppo tramite l'interfaccia web. Approfitta di queste preziose funzionalità.

