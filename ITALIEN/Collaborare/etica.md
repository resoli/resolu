---
title: Etica
---

Perché la scelta del software libero è un problema per la società?
Può l'Economia Sociale e Solidale (ESS) difendere valori etici senza tener conto di quelli promossi dagli strumenti digitali che usa?
{: .slogan-subtitle }

## La scelta di uno strumento digitale è una questione etica

Quando ci interroghiamo sui valori che guidano le nostre azioni e i nostri comportamenti nella società, a volte arriviamo al punto di cercare di decidere cosa è giusto e cosa è sbagliato. Facciamo attenzione a non percorrere quella distanza, che porta a questioni di moralità.

La riflessione sulle questioni etiche dell'ambiente digitale non intende stabilire una definizione assoluta di usi buoni e cattivi. 
Si tratta piuttosto di saper scegliere gli strumenti che ci permettono di agire secondo i valori che, per noi, miglio­rano la vita nella società.
{: .encart }

L'uso degli strumenti digitali ha un impatto che va ben oltre il mondo digitale. Li usiamo per informarci, per comunicare, per organizzarci, cioè per agire con gli altri. Noi interagiamo, interpretiamo delle informazioni e organizziamo dei collettivi attraverso molteplici servizi e software. 

In questo contesto, il controllo sulle nostre azioni e decisioni è possibile solo se abbiamo il potere di controllare il software su cui si basano. Quali sono le condizioni necessarie per mantenere il controllare i nostri strumenti e agire secondo i nostri valori? Consideriamo come tutto dipende dal nostro grado di libertà e sovranità rispetto ai nostri strumenti digitali.

## I valori promossi dal software libero

Come per le organizzazioni dell'ESS, l'etica del software libero si trova nello scopo del movimento e nella sua governance. 

Le quattro libertà che definiscono il software libero sono quelle dei suoi utenti: siete liberi di usare, studiare, copiare, modificare e ridistribuire tutto il software libero del mondo. Sulla base di queste quattro libertà, il FOSS rafforza l'accesso universale agli strumenti digitali, il controllo degli utenti sui loro strumenti, la trasparenza del loro funzionamento e del trattamento dei dati personali, l'indipendenza di ciascuno di essi dalle società di software proprietario.
{: .encart }

Oltre al rispetto della libertà di tutti, il FOSS è caratterizzato da una modalità di gestione partecipativa e democratica; chiunque voglia contribuire può farlo, e quindi entrare a far parte di una comunità di sviluppatori. Questo funzionamento assicura una governance orizzontale degli strumenti sviluppati, che sono gestiti come [beni comuni digitali](../Organizzare/comuni.md). 

Il software Libero incoraggia anche la condivisione della conoscenza, la messa in comune delle idee e l'apertura all'innovazione, a beneficio di tutti: qualsiasi idea implementata in un software libero sarà accessibile a tutto il mondo e potrà essere riutilizzata per lo sviluppo di altri strumenti. Questo approccio solidale mira a far fiorire l'intelligenza collettiva nel mondo digitale, in modo che tutti possano utilizzarla e contribuirvi.


L'etica del software libero si basa quindi sul rispetto della libertà del suo utilizzo, sulla governance democratica, sull'emancipazione degli attori che concentrano il controllo sugli strumenti proprietari, ecc. ([privacy](../Comunicare/privacy.md)), la solidarietà tra utenti e collaboratori e la condivisione della conoscenza.

Scegliere uno strumento digitale significa collocarsi in un modello di società che si vuole incoraggiare. E' quello di un modello di mercato basato sulla rivendita dei dati personali, sulla chiusura dell'innovazione e sulla delega del controllo sul funzionamento e sull'evoluzione degli strumenti digitali a un numero ristretto di attori molto potenti? O è piuttosto quello di un modello i cui valori riecheggiano punto per punto quelli dell'Economia Sociale e Solidale?

## La posta in gioco della libertà digitale per l'economia sociale e solidale

Le organizzazioni dell'ESS sono legate all'etica delle loro azioni e alla loro utilità sociale. I metodi di gestione partecipativa e i modelli economici utilizzati riflettono la volontà di far progredire la società verso una maggiore giustizia, coesione sociale e solidarietà.

Se questo approccio cittadino è radicato nei luoghi, negli eventi e negli incontri, si realizza grazie agli strumenti digitali che utilizziamo per comunicare, informare, organizzare e mobilitare. È quindi essenziale che coloro che sono coinvolti in questo processo siano in grado di mantenere il controllo degli strumenti che permettono loro di agire, e di incoraggiare un modello che garantisca il controllo democratico.

Decidendo di orientare l'ecosistema digitale verso una maggiore libertà per tutti gli utenti, le organizzazioni dell'ESS si assicurano sia la loro sovranità che il loro potere di agire con strumenti sviluppati nel loro interesse. 

La scelta di uno strumento digitale è una scelta di società: i vostri usi digitali sono altrettante scelte che rafforzano o limitano i valori etici condivisi di ESS e Libertà.

