---
title: Zourit.net (resoconto)
---

I software liberi nell’Educazione Popolare e non solo...
{: .slogan-subtitle }

Zourit (la piovra  in creolo reunionese) non è probabilmente il software del secolo, ma l'approccio che ha portato alla sua creazione può essere considerato esemplare in termini di valori dell'Educazione Popolare e della Libertà in generale. Esso è una perfetta illustrazione di ciò di cui stiamo parlando in queste pagine in termini di scelta e gestione del cambiamento.

## Inizialmente, un processo interno...

Diversi anni fa, CEMÉA ha iniziato un lungo e profondo processo di riflessione e formazione sull'uso degli strumenti digitali, che ha portato allo sviluppo di un "Manifesto dei Sistemi Informativi" che ci impone di "ripensare i nostri strumenti e le nostre pratiche digitali". 

Questo manifesto ha portato il CEMÉA a considerare (dato che non siamo riusciti a trovare la nostra "felicità" nell'offerta esistente) lo sviluppo del software necessario per il funzionamento di una struttura associativa: e-mail, contatti, agende condivise, cloud, videoconferenze, contabilità e gestione dei soci, ecc. Tutto questo doveva essere completamente gratuito (sotto licenza GPL), ospitato il più vicino possibile agli utenti e naturalmente rispettoso della privacy.

## ... ma anche politico ed educativo

Zourit ora equipaggia quasi tutte le associazioni territoriali CEMÉA.  Ognuno di essi è stato oggetto di formazione dedicata per accompagnare questo cambiamento con la nostra  lavoratori e militanti. 

Noi abbiamo pensato di lasciare le cose come stanno, informando la disponibilità delle fonti, libero a chiunque di adottarlo! Numerosi associazioni ci hanno pregato di non creare un proprio server, ma di beneficiare dell'hosting. 

È infine quello che abbiamo fatto di recente diventando membri del COLLETTIVO CHATONs (vedi [chatons.org](http://chatons.org/)) sotto il nome di [zourit.net](http://zourit.net/) e offrendo non solo i servizi Zourit, ma anche e soprattutto un accompagnamento delle associazioni a questi utilizzi.

Il fatto che questo strumento sia stato sviluppato sotto la licenza GPL, indica in modo che in nessun caso proponiamo alle associazioni o le autorità locali a ricadere in un'altra forma di dipendenza. Non appena lo desiderano, potranno fare tutto quello che vogliono per recuperare i loro dati, ma, cosa ancora più importante, tutti sono liberi di installare ed eventualmente amministrare il proprio server Zourit. Vogliamo dare l'impulso e dimostrare la fattibilità di un'altra possibile gestione del "digitale", più vicino all'utente e portatore di legami sociali  incoraggiando la creazione di molti CHATONS Zourit.


## Ma che cosa fa Zourit?

I software o i servizi offerti sono presentati in dettaglio anche in queste schede! Possiamo comunque citare un importante riferimento alle funzionalità di Nextcloud, alla gestione delle mailing list, alla gestione della contabilità associativa, al mailing (con Zimbra), alle videoconferenze, e tutto questo con un facile accesso su smartphone.

Per permetterci di decidere liberamente il nostro destino collettivo, il controllo democratico della nostra società digitale e dei suoi usi diventa il tema centrale di ogni azione di Educazione Popolare. Attraverso un progetto come questo, stiamo contribuendo a inventare il futuro digitale che vogliamo, non quello che ci sarà lasciato!

Per maggiori informazioni è possibile può contattare : zourit@cemea.org
{: .slogan-contenu }
