---
title: La confidentialité
---

Pourquoi la confidentialité est importante ? Comment utiliser les outils
numériques tout en respectant sa vie privée et celle des autres ?
{: .slogan-subtitle }

## Qu'est-ce qu'une donnée personnelle ?

Les données personnelles sont toutes les informations qui concernent une
personne physique. Elles permettent de l'identifier de manière directe
(le nom, le prénom) ou de manière indirecte (une adresse postale, un
numéro de téléphone, une adresse IP, etc.)

On peut identifier quelqu'un·e en rassemblant des données personnelles
grâces à de multiples techniques de recoupement de données
comportementales (y compris les comportements sur Internet) et en
croisant des informations en apparence anodines (notamment les données
de clientèle, les inscriptions sur des services numériques en ligne,
etc.).

## Pourquoi s'en préoccuper ?

Les données personnelles sont très utiles pour des entreprises comme les
géants du Web (les fameux GAFAM notamment), car il s'agit en fait du
véritable produit de ces entreprises. Leur but n'est pas de fournir un
service qui convient aux utilisateur⋅rice⋅s, mais de concevoir des
services qui leur permettent de **récolter le plus d'informations
personnelles possible**. Ces données sont le plus
souvent revendues à des **annonceurs**, qui contribuent
à une économie basée sur la publicité.

## Savoir choisir

Le choix des logiciels et des services que vous utilisez au sein de
votre organisation est donc un choix stratégique. **Vous devez
protéger** les données de vos adhérent·es, celles des
visiteu·se·s de vos sites web, celles des utilisateur·rice·s de vos
services. C'est non seulement un devoir rendu obligatoire par la loi,
comme la mise en conformité avec le règlement général sur la protection
des données (RGPD), mais aussi un devoir moral qui consiste à ne pas
exposer l'intimité de vos collaborateur·rice·s ou de vos visiteur·se·s
(et la loi ne définit pas tout). Vous devez donc vous assurer du degré
de **confiance** que vous placez dans les solutions
numériques que vous utilisez et du degré de
**confiance** que vous prêtez aux hébergeurs ou
fournisseurs avec qui vous travaillez.

## « Ok, mais moi ça ne me dérange pas »…

Cela peut en revanche poser problème **aux autres**
personnes touchées, que ce soit des membres de la structure ou des
personnes extérieures qui auraient transmis leurs informations. Par
exemple, pour une association d'aide à des personnes en situation de
difficulté sociale ou de santé, ou pour une association de militant⋅e⋅s
politiques, donner leurs informations à des acteurs privés peut être
problématique.

## … ou bien : « je n'ai rien à cacher »

Lorsque vous allez aux toilettes, vous fermez la porte. Chacun possède
une **intimité** et nous avons potentiellement une
**intimité numérique**. L'intimité est un besoin vital
et participe à notre construction en tant qu'êtres humains.

Protéger son intimité numérique, c'est aussi protéger celle des autres.
{: .slogan-contenu }

Toutes les traces numériques que nous laissons peuvent être analysées de
manière automatique par des procédés d'exploration de données à grande
échelle réalisés par des entreprises qui captent des données en masse,
élaborent des profils et sont capables d'orienter nos comportements
économiques et sociaux, voire nos choix politiques (sur ce point, on
peut se référer à l'affaire Cambridge Analytica qui éclata en 2018).
