---
title: Gratuité
---

Les logiciels libres sont gratuits, comment est-ce possible ?<br>
Le sont-ils toujours ?
{: .slogan-subtitle }

## Des logiciels déjà payés

Les logiciels libres sont gratuits car ils ont **déjà été payés**. En
consacrant du temps et de l'argent à leur développement, les
**contributeur·rice·s** participent ensemble à l'élaboration de ces
outils afin de les mettre à disposition de tout le monde. C'est ainsi
que des logiciels comme LibreOffice (suite bureautique) ou The GIMP
(édition et retouche d'image) permettent sensiblement la même chose que
leurs alternatives payantes. Des projets comme Wikipédia ou Framasoft
vivent uniquement grâce aux dons des utilisateur⋅rice⋅s qui n'achètent pas de
produit mais participent à une économie alternative pour entretenir des
[communs numériques](../organiser/communs.md).

## Des services en ligne

Certains logiciels sont des **services en ligne**. Ils
permettent d'accéder à des outils avec un navigateur sans avoir besoin
d'installer le logiciel sur son ordinateur. Afin de mettre à disposition
de tels services, l'hébergeur a besoin de serveurs et de temps pour s'en
occuper. Si certains outils sont peu coûteux à mettre en place, d'autres
peuvent demander plus de ressources. Par exemple la mise à disposition
d'un espace de stockage requiert d'avoir suffisamment de disques durs
pour tout stocker et assurer des sauvegardes, voire de sous-traiter
l'infrastructure technique auprès d'un fournisseur de serveurs.

Les hébergeurs professionnels, même s'ils proposent des services basés
sur des logiciels libres, doivent à minima faire payer l'hébergement
pour couvrir leurs coûts d'infrastructures et les salaires. De même, les
hébergeurs associatifs, qu'ils emploient des professionnel⋅le⋅s ou non,
peuvent demander une une participation financière ou inciter à effectuer
un don. Ce n'est pas le logiciel libre que l'on paye, mais le service.
{: .encart }

## Mais Google c'est gratuit

Et pourtant, Google est l'une des entreprises les plus riches au monde.
Lorsqu'on utilise les services gratuits d'une telle entreprise, on ne
paye pas en argent mais en attention et en données personnelles. Les
véritables clients des GAFAM ne sont pas les utilisateur⋅rice⋅s des
services gratuits, mais les annonceurs à qui sont vendus des espaces
publicitaires ou les courtiers de données qui, grâce à leurs techniques
d'exploration de données, effectuent des plus-values énormes en
revendant des profils à d'autres clients.

La récolte, l'extraction et l'exploitation des données personnelles
posent de sérieux problèmes de confidentialité et participent à une
économie où peu d'acteurs accaparent un grand pouvoir. Grâce à leurs
services qu'on ne paie pas avec de l'argent, certaines entreprises se
sont rendues presque indispensables dans nos vies quotidiennes. Nous
leur conférons sans vraiment le vouloir un pouvoir énorme sur beaucoup
d'aspects de nos vies publiques ou privées.
{: .encart }

## Pourquoi payer pour des services ?

L'hébergement de services en ligne a un coût. Ces services tournent sur
les ordinateurs de l'hébergeur, qui doit assurer les coûts matériels
(serveurs, disques durs, etc.), la connexion Internet, mais aussi la
main-d'œuvre pour la mise à disposition et la maintenance, la
sécurisation des données, etc.

Rétribuer un hébergeur éthique, c'est contribuer à une économie
solidaire, basée sur l'échange et le partage de compétences.

Passer par une structure plus humaine permet non seulement la création
de lien social, mais aussi d'agir directement sur les outils que l'on
utilise afin de les faire évoluer en fonction de ses besoins. Ainsi il
n'est pas rare qu'une entreprise ou une association spécialisée en
logiciels libres contribue aussi au développement des logiciels qu'elle
utilise. La boucle a tendance à être vertueuse.

Il existe beaucoup d'hébergeurs éthiques. Par exemple, les CHATONS
s'engagent via un manifeste et une charte commune à respecter les
utilisateur·rice·s tout en offrant de manière transparente des services
libres à un prix raisonnable et en adéquation avec les coûts de mise en
œuvre. Vous pouvez trouver le CHATONS qui vous convient sur
[chatons.org](http://chatons.org/) !
{: .encart }

## +/-

Payer ou ne pas payer est un choix qui se mesure à l'aune de la
confiance qu'on a envers l'autre partie. Une solution propriétaire
gratuite peut devenir payante du jour au lendemain, et les
utilisateur⋅rice⋅s se retrouvent alors captif⋅ve⋅s d'un choix
qu'il⋅elle⋅s n'ont pas voulu faire au départ.

Payer pour un service libre peut paraître étrange : enfin, vous savez ce
vous payez et pourquoi vous le payez !
{: .slogan-contenu }

On peut mentionner l'exemple du service Doodle fourni par la société
suisse Doodle AG. Cette société fournissait gratuitement un outil de
sondage et d'agenda collaboratif célèbre et vendait de l'espace
publicitaire. En 2019, elle a décidé subitement de rendre ce service
payant avec une version d'essai très restreinte. Les utilisateur⋅rice⋅s
qui avaient pris l'habitude d'utiliser ce service pendant des années ont
alors dû faire un choix contraint. L'association Framasoft propose
depuis longtemps une alternative très efficace nommée Framadate…
{: .encart }
