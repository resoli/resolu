#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from slugify import slugify
from pathlib import Path

BASE_DIR = Path('.')

def clean_html():
    """WIP : clean full html export"""
    with open(BASE_DIR / 'full' / 'full.html') as fp:
        soup = BeautifulSoup(fp)

def split_full():
    """Split full mkardown content by parts"""
    with open(BASE_DIR / 'full' / 'full.md', 'r') as f:
        file_index = 1
        current_dirname = ''
        current_filename = 's{0}-first.md'.format(file_index)
        outfile = open(BASE_DIR / 'splited' / current_filename, 'a')
        for l in f.readlines():
            if l.startswith('#'):
                outfile.close()
                file_index += 1
                current_filename = 's{0}-{1}.md'.format(file_index, slugify(l))
                if l.startswith('# '):
                    current_dirname = l.lstrip('# ').rstrip(' \n').lower()
                    if not ((BASE_DIR / 'splited' / current_dirname).exists()):
                        (BASE_DIR / 'splited' / current_dirname).mkdir()
            outfile = open(BASE_DIR / 'splited' / current_dirname / current_filename, 'a')
            outfile.write(l)

# clean_html()
split_full()
