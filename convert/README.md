# Convertir le fichier odt en markdown

Dans le dossier `convert`

- Export du fichier odt vers `full.html`

- Nettoyer les noms de classe, les div vides, etc. (script à faire ?)

- Exporter le fichier `full.html` en markdown

    ```
    pandoc --atx-headers full.html -o full.md
    ```

- Scinder les fichiers par partie

    ```
    ./splitmd.py
    ```

PS : regex utiles

échanger texte-important en emphase forte : `\[([a-zéèà⋅' \n]+)\]\{\.texte-important\}`
